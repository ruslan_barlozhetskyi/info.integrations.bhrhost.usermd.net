<?php
$file = 'request.log';

$current = file_get_contents($file);


function decodePost($post){
    $decodedPost = json_decode($post, true);

    return $decodedPost['pullrequest']['source']['repository']['links']['self']['href'];
}


$current .= decodePost($_POST);


file_put_contents($file, $current);